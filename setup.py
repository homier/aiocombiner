#!/usr/bin/env python3
import setuptools


BASE_REQUIREMENTS = [
    'aiohttp==3.5.4',
    'environs==4.1.0',
    'pyyaml==5.1',
]


DEV_REQUIREMENTS = [
    'asynctest==0.12.2',
    'flake8==3.7.7',
    'pytest-asyncio==0.10.0',
    'pytest-cov==2.6.1',
    'pytest-mock==1.10.1',
    'pytest-runner==4.4',
    'pytest==4.3.1',
]


SETUP_REQUIREMENTS = [
    'setuptools-scm==3.2.0',
]


UVLOOP_REQUIREMENTS = [
    'uvloop==0.12.1',
]


setuptools.setup(
    name='aiocombiner',
    description='asyncio application foundation',
    url='https://gitlab.com/homier/aiocombiner',
    author='Dzmitry Mikhalapau',
    author_email='hack.you1956@hotmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Framework :: AsyncIO',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    packages=setuptools.find_packages(
        exclude=[
            'docs',
            'examples',
            'tests',
        ],
    ),
    install_requires=BASE_REQUIREMENTS,
    extras_require={
        'dev': DEV_REQUIREMENTS,
        'uvloop': UVLOOP_REQUIREMENTS,
    },
    setup_requires=SETUP_REQUIREMENTS,
    version=None,
    use_scm_version={'root': '.', 'relative_to': __file__},
)
