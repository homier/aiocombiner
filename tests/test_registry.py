import typing

import pytest

from aiocombiner.exception import ApplicationError
from aiocombiner.service import Service


def test_service_registry_service_not_found(service_registry):
    with pytest.raises(ApplicationError):
        service_registry['test']


def test_service_registry_set_incorrect_service_type(service_registry):
    with pytest.raises(TypeError):
        service_registry['key'] = 1


def test_service_registry_set_correct_service_type(service_registry, test_service):
    try:
        service_registry['test'] = test_service
    except TypeError as e:
        pytest.fail("There should be no TypeError: {!r}".format(e))


def test_service_registry_set_dublicate_service(service_registry, test_service):
    service_registry['test'] = test_service

    with pytest.raises(ApplicationError):
        service_registry['test'] = test_service


def test_service_registry_service_found(service_registry, test_service):
    service_registry['test'] = test_service

    assert service_registry['test'] == test_service


def test_service_registry_services_returns_list(service_registry, test_service):
    service_registry['test'] = test_service

    assert isinstance(service_registry.services(), typing.List)


def test_service_registry_services_list_contains_of_services(
    service_registry,
    test_service,
):
    service_registry['test'] = test_service

    for service in service_registry.services():
        assert isinstance(service, Service)
