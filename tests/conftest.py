import asynctest
import pytest

from aiocombiner import registry
from aiocombiner.application import Application
from aiocombiner.service import Service
from aiocombiner.settings import SettingsSchema


@pytest.fixture
def amock():
    return asynctest.CoroutineMock


@pytest.fixture(scope='function')
def service_registry():
    return registry.ServiceRegistry()


class TestService(Service):

    name = 'test'

    async def on_startup(self):
        pass

    async def on_stopping(self):
        pass


@pytest.fixture(scope='function')
def test_service() -> TestService:
    return TestService()


class TestApplication(Application):
    name = 'test'
    services = [TestService]


@pytest.fixture(scope='function')
def test_application() -> TestApplication:
    return TestApplication()


class TestSettings(SettingsSchema):
    environment_prefix = 'test'


@pytest.fixture(scope='function')
def test_settings() -> TestSettings:
    return TestSettings()
