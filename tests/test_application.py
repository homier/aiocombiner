import asyncio
import os
import sys

import aiohttp.web
import pytest

from aiocombiner import application
from aiocombiner import exception


def test_application_name_none():

    class Application(application.Application):
        pass

    with pytest.raises(exception.ApplicationError):
        Application()


def test_application_name_empty_string():

    class Application(application.Application):
        name = ''

    with pytest.raises(exception.ApplicationError):
        Application()


def test_application_name_not_string():

    class Application(application.Application):
        name = 1

    with pytest.raises(TypeError):
        Application()


def test_init_calls_required_initializers(test_application, mocker):
    init_loop_mocked = mocker.patch.object(test_application, '_init_loop')
    init_aiohttp_application_mocked = mocker.patch.object(
        test_application, '_init_aiohttp_application')
    init_routes_mocked = mocker.patch.object(test_application, '_init_routes')
    init_services_mocked = mocker.patch.object(test_application, '_init_services')

    test_application._init()

    init_loop_mocked.assert_called_once()
    init_aiohttp_application_mocked.assert_called_once()
    init_routes_mocked.assert_called_once()
    init_services_mocked.assert_called_once()


def test_init_loop_calls_requires_setters(test_application, mocker):
    set_uvloop_policy_mocked = mocker.patch.object(
        test_application, '_set_uvloop_policy')
    set_loop_mocked = mocker.patch.object(
        test_application, '_set_loop')
    set_thread_pool_executor_mocked = mocker.patch.object(
        test_application, '_set_thread_pool_executor')

    test_application._init_loop()

    set_uvloop_policy_mocked.assert_called_once()
    set_loop_mocked.assert_called_once()
    set_thread_pool_executor_mocked.assert_called_once()


def test_set_uvloop_policy_use_uvloop_not_set(test_application, mocker):
    mocker.patch.dict('os.environ', {})

    set_policy_mocked = mocker.patch.object(asyncio, 'set_event_loop_policy')
    test_application._set_uvloop_policy()
    set_policy_mocked.assert_not_called()


def test_set_uvloop_policy_uvloop_package_not_found(test_application, mocker):
    mocker.patch.dict('os.environ', {'USE_UVLOOP': '1'})

    set_policy_mocked = mocker.patch.object(asyncio, 'set_event_loop_policy')
    mocker.patch.dict('sys.modules', {'uvloop': None})
    test_application._set_uvloop_policy()
    set_policy_mocked.assert_not_called()

    mocker.resetall()


def test_set_uvloop_policy_uvloop_found(test_application, mocker):

    uvloop = mocker.MagicMock()
    uvloop.EventLoopPolicy.return_value = "some shit"

    mocker.patch.dict('os.environ', {'USE_UVLOOP': '1'})
    mocker.patch.dict('sys.modules', {'uvloop': uvloop})

    set_policy_mocked = mocker.patch.object(asyncio, 'set_event_loop_policy')
    test_application._set_uvloop_policy()
    set_policy_mocked.assert_called_once()

    mocker.resetall()


def test_set_loop_sets_loop(test_application, mocker):
    get_event_loop_mocked = mocker.patch.object(
        asyncio, 'get_event_loop', return_value=1)

    test_application._set_loop()

    get_event_loop_mocked.assert_called_once()
    test_application._loop == 1


def test_set_thread_pool_executor(test_application, mocker):
    thread_pool_executor_mocked = mocker.patch(
        'concurrent.futures.ThreadPoolExecutor', return_value=1)

    test_application._loop = mocker.MagicMock()
    test_application._loop.set_default_executor.return_value = None

    test_application._set_thread_pool_executor()
    test_application._loop.set_default_executor.assert_called_once_with(1)
    thread_pool_executor_mocked.assert_called_once_with()


def test_init_aiohttp_application(test_application):
    test_application._init_aiohttp_application()

    assert test_application.aiohttp_app is not None
    assert isinstance(test_application.aiohttp_app, aiohttp.web.Application)
    assert hasattr(test_application.aiohttp_app, 'combiner')
    assert test_application.aiohttp_app.combiner is not None
    assert isinstance(test_application.aiohttp_app.combiner, application.Application)


def test_init_routes_incorrect_route_type(test_application, mocker):
    test_application.routes = [1]
    test_application._init_aiohttp_application()

    add_routes_mocked = mocker.patch.object(
        test_application.aiohttp_app, 'add_routes')

    with pytest.raises(TypeError):
        test_application._init_routes()

    add_routes_mocked.assert_not_called()


def test_init_routes_calls_add_routes(test_application, mocker):
    test_application.routes = []
    test_application._init_aiohttp_application()

    add_routes_mocked = mocker.patch.object(
        test_application.aiohttp_app, 'add_routes')

    test_application._init_routes()
    add_routes_mocked.assert_called_once_with(test_application.routes)


def test_init_service(test_application, test_service, mocker):
    klass = test_service.__class__
    set_application_mocked = mocker.patch.object(klass, 'set_application')
    test_application._init_service(klass)
    set_application_mocked.assert_called_once_with(test_application)
    assert test_service.name in test_application._service_registry

    mocker.resetall()


def test_handle_main_error_does_not_raise(test_application, mocker):

    future = mocker.MagicMock()

    future.result.side_effetct = Exception

    try:
        test_application._handle_main_error(future)
    except Exception as e:
        pytest.fail("There should be no Exception: {!r}".format(e))


@pytest.mark.asyncio
async def test_run_in_thread_calls_run_in_executor(test_application, mocker):
    test_application._init_loop()

    func = mocker.MagicMock()
    func.func.return_value = 'test'

    result = await test_application.run_in_thread(func.func)

    assert result == 'test'


def test_app_pid_return_correct_pid(test_application):
    assert test_application.app_pid == os.getpid()


@pytest.mark.asyncio
async def test_on_app_cleanup_already_not_running(test_application, mocker, amock):
    shutdown_services_mocked = mocker.patch.object(
        test_application, '_shutdown_services', amock())
    shutdown_pending_tasks_mocked = mocker.patch.object(
        test_application, '_shutdown_pending_tasks', amock())

    test_application._is_running = False

    await test_application.on_app_cleanup(test_application)

    shutdown_services_mocked.assert_not_called()
    shutdown_pending_tasks_mocked.assert_not_called()


@pytest.mark.asyncio
async def test_on_app_cleanup_sets_not_running(test_application, mocker, amock):
    mocker.patch.object(test_application, '_shutdown_services', amock())
    mocker.patch.object(test_application, '_shutdown_pending_tasks', amock())

    test_application._is_running = True

    await test_application.on_app_cleanup(test_application)

    assert not test_application._is_running


@pytest.mark.asyncio
async def test_on_app_cleanup_calls_shutdown_methods(test_application, mocker, amock):
    shutdown_services_mocked = mocker.patch.object(
        test_application, '_shutdown_services', amock())
    shutdown_pending_tasks_mocked = mocker.patch.object(
        test_application, '_shutdown_pending_tasks', amock())

    test_application._is_running = True

    await test_application.on_app_cleanup(test_application)

    shutdown_services_mocked.assert_called_once()
    shutdown_pending_tasks_mocked.assert_called_once()


def test_set_main_task(test_application, mocker):
    mocked_task = mocker.Mock()
    mocked_task.add_done_callback = mocker.Mock()
    ensure_future_mocked = mocker.patch(
        'asyncio.ensure_future', mocker.MagicMock(return_value=mocked_task))
    application_main_mocked = mocker.patch.object(
        test_application, 'main', mocker.MagicMock(return_value='kinda future'))

    test_application._set_main_task()

    mocked_task.add_done_callback.assert_called_once_with(
        test_application._handle_main_error)
    application_main_mocked.assert_called_once()
    ensure_future_mocked.assert_called_once_with('kinda future')

    mocker.resetall()


def test_init_base_application_settings_env_priority(test_application, mocker):
    mocker.patch.dict(os.environ, {'TEST_HOST': 'test', 'TEST_PORT': '1234'})

    test_application._init_base_application_settings()

    assert test_application._host == 'test'
    assert test_application._port == 1234

    mocker.resetall()


def test_init_base_application_settings_defaults(test_application):
    test_application._init_base_application_settings()

    assert test_application._host == application.DEFAULT_APPLICATION_BINDING_HOST
    assert test_application._port == application.DEFAULT_APPLICATION_BINDING_PORT


def test_init_base_application_settings_from_file(test_application, mocker):
    mocker.patch.dict(test_application._raw_settings, {'host': 'test', 'port': 1234})

    test_application._init_base_application_settings()

    assert test_application._host == 'test'
    assert test_application._port == 1234

    mocker.resetall()


def test_init_base_application_settings_from_env_and_file(test_application, mocker):
    mocker.patch.dict(test_application._raw_settings, {'host': 'test'})
    mocker.patch.dict(os.environ, {'TEST_PORT': '1234'})

    test_application._init_base_application_settings()

    assert test_application._host == 'test'
    assert test_application._port == 1234

    mocker.resetall()


def test_init_base_application_settings_only_from_env(test_application, mocker):
    mocker.patch.dict(os.environ, {'TEST_HOST': 'env', 'TEST_PORT': '1234'})
    mocker.patch.dict(test_application._raw_settings, {'host': 'file', 'port': '2345'})

    test_application._init_base_application_settings()

    assert test_application._host == 'env'
    assert test_application._port == 1234

    mocker.resetall()


def test_load_settings_from_file_path_is_not_set(test_application, mocker):
    env_mocked = mocker.patch(
        'aiocombiner.environ.ENV', mocker.MagicMock(return_value=None))

    test_application._raw_settings = None
    test_application._load_settings_from_file()

    assert test_application._raw_settings is None
    env_mocked.assert_called_once_with('AIOCOMBINER_SETTINGS_PATH', None)

    mocker.resetall()


def test_load_settings_from_file_path_zero_length(test_application, mocker):
    env_mocked = mocker.patch(
        'aiocombiner.environ.ENV', mocker.MagicMock(return_value=''))

    test_application._raw_settings = None
    test_application._load_settings_from_file()

    assert test_application._raw_settings is None
    env_mocked.assert_called_once_with('AIOCOMBINER_SETTINGS_PATH', None)
    mocker.resetall()

def test_load_settings_from_file_path_is_set(test_application, mocker):
    env_mocked = mocker.patch(
        'aiocombiner.environ.ENV', mocker.MagicMock(return_value='test'))
    load_raw_settings_mocked = mocker.patch.object(
        test_application, '_load_raw_settings')
    mocker.patch('os.path.exists', mocker.MagicMock(return_value=True))

    test_application._load_settings_from_file()

    env_mocked.assert_called_once_with('AIOCOMBINER_SETTINGS_PATH', None)
    load_raw_settings_mocked.assert_called_once_with('test')

    mocker.resetall()
