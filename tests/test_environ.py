from aiocombiner import environ


def test_environ_environ_context_manager_returns_env():
    with environ.environ('test') as env:
        assert env is environ.ENV


def test_environ_transform_prefix():
    assert environ.transform_prefix('test') == 'TEST_'


def test_environ_environ_calls_prefixed_correctly(mocker):
    prefixed_mocked = mocker.patch.object(environ.ENV, 'prefixed')

    with environ.environ('test'):
        pass

    prefixed_mocked.assert_called_once_with("TEST_")

    mocker.resetall()
