import typing

import pytest

from aiocombiner import util


def test_check_type_correct_type():
    try:
        util.check_type('number', 1, int)
    except TypeError as e:
        pytest.fail("There should be no {!r} error".format(e))


def test_check_type_correct_types():
    try:
        util.check_type('number', 1, object)
    except TypeError as e:
        pytest.fail("There should be no {!r} error".format(e))


def test_check_type_at_least_one_type_is_correct():
    try:
        util.check_type('number', 1, str, int)
    except TypeError as e:
        pytest.fail("There should be no {!r} error".format(e))


def test_check_type_incorrect_type():
    with pytest.raises(TypeError):
        util.check_type('string', 1, str)


def test_check_type_incorrect_types():
    with pytest.raises(TypeError):
        util.check_type('dict', 'str', typing.Callable, typing.Dict)
