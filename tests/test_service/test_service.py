import abc

import pytest

from aiocombiner.service import service
from aiocombiner.settings import SettingsSchema


def test_service_name_is_none():

    class Service(service.Service):
        async def on_startup(self):
            pass

        async def on_stopping(self):
            pass

    with pytest.raises(AssertionError):
        Service()


def test_service_name_empty_string():

    class Service(service.Service):

        name = ''

        async def on_startup(self):
            pass

        async def on_stopping(self):
            pass

    with pytest.raises(AssertionError):
        Service()


def test_service_name_not_string():

    class Service(service.Service):

        name = 1

        async def on_startup(self):
            pass

        async def on_stopping(self):
            pass

    with pytest.raises(TypeError):
        Service()


def test_service_abstract():
    with pytest.raises(TypeError):
        service.Service()


def test_service_without_implemented_methods():

    class Service(service.Service):
        pass

    with pytest.raises(TypeError):
        Service()


def test_service_method_on_stopping_is_not_implemented():

    class Service(service.Service):
        async def on_startup(self):
            pass

    with pytest.raises(TypeError):
        Service()


def test_service_method_on_startup_is_not_implemented():

    class Service(service.Service):
        async def on_stopping(self):
            pass

    with pytest.raises(TypeError):
        Service()


def test_service_both_methods_are_implemented():

    class Service(service.Service):
        name = 'test'

        async def on_startup(self):
            pass

        async def on_stopping(self):
            pass

    try:
        Service()
    except TypeError as e:
        pytest.fail("There should be no TypeError: {!r}".format(e))


def test_service_is_configurable_false_not_raises_exceptions():

    class Service(service.Service):
        name = 'test'
        is_configurable = False

        async def on_startup(self):
            pass

        async def on_stopping(self):
            pass

    try:
        Service()
    except AssertionError as e:
        pytest.fail("There should be no AssertionError: {!r}".format(e))
    except TypeError as e:
        pytest.fail("There should be no TypeError: {!r}".format(e))


def test_service_is_configurable_true_schema_missed():

    class Service(service.Service):
        name = 'test'
        is_configurable = True

        async def on_startup(self):
            pass

        async def on_stopping(self):
            pass

    with pytest.raises(AssertionError):
        Service()


def test_service_is_configurable_true_schema_incorrect_type():

    class Service(service.Service):
        name = 'test'
        is_configurable = True
        settings_schema = 'Something'

        async def on_startup(self):
            pass

        async def on_stopping(self):
            pass

    with pytest.raises(TypeError):
        Service()


def test_service_is_configurable_true_schema_correct_type():

    class Service(service.Service):
        name = 'test'
        is_configurable = True
        settings_schema = SettingsSchema()

        async def on_startup(self):
            pass

        async def on_stopping(self):
            pass

    try:
        Service()
    except AssertionError as e:
        pytest.fail("There should be no AssertionError: {!r}".format(e))
    except TypeError as e:
        pytest.fail("There should be no TypeError: {!r}".format(e))


def test_service_default_state(test_service):
    assert test_service.state is service.ServiceState.UNKNOWN


def test_service_set_application(test_service):
    test_service.set_application('test')
    assert test_service.application == 'test'


def test_service_set_state(test_service, mocker):
    mocked_validate_state_for_setting = mocker.patch.object(
        test_service, '_validate_state_for_setting')

    test_service.set_state(service.ServiceState.STARTED)
    mocked_validate_state_for_setting.assert_called_once_with(
        service.ServiceState.STARTED)
    assert test_service.state is service.ServiceState.STARTED


def test_service_set_settings_not_configurable(test_service, mocker):
    _set_settings_mocked = mocker.patch.object(test_service, '_set_settings')

    test_service.set_settings()

    _set_settings_mocked.assert_not_called()
    mocker.resetall()


def test_service_set_settings_calls_set_settings(test_service, mocker):
    _set_settings_mocked = mocker.patch.object(test_service, '_set_settings')
    test_service.is_configurable = True

    test_service.set_settings()

    test_service.is_configurable = False
    _set_settings_mocked.assert_called_once_with(initial_settings=None)
    mocker.resetall()


def test_service_set_settings_replace_none_with_dict(test_service, mocker):
    test_service.settings_schema = mocker.MagicMock()
    test_service.settings_schema.load.return_value = (None, None)

    test_service._set_settings()

    test_service.settings_schema.load.assert_called_once_with({})
    mocker.resetall()


def test_service_set_settings_incorrect_input(test_service, mocker):
    test_service.settings_schema = mocker.MagicMock()
    test_service.settings_schema.load.return_value = (None, None)

    with pytest.raises(TypeError):
        test_service._set_settings(initial_settings=123)

    test_service.settings_schema.load.assert_not_called()
    mocker.resetall()


def test_validate_state_for_setting_unexisting_state(test_service):
    with pytest.raises(service.ServiceStateError):
        test_service._validate_state_for_setting('something')


def test_validate_state_for_setting_ordering_unknown(test_service):
    try:
        for state in service._ALL_STATES:
            test_service.state = state
            test_service._validate_state_for_setting(service.ServiceState.UNKNOWN)
    except service.ServiceStateError as e:
        pytest.fail("There should be no ServiceStateError: {!r}".format(e))


def test_validate_state_for_setting_ordering_failed(test_service):
    states = [
        state
        for state in service._ALL_STATES
        if state is not service.ServiceState.STARTED
    ]

    for state in states:
        test_service.state = state
        with pytest.raises(service.ServiceStateError):
            test_service._validate_state_for_setting(service.ServiceState.FAILED)

    try:
        test_service.state = service.ServiceState.STARTED
        test_service._validate_state_for_setting(service.ServiceState.FAILED)
    except service.ServiceStateError as e:
        pytest.fail("There should be no ServiceStateError: {!r}".format(e))


def test_validate_state_for_setting_ordering_started(test_service):
    states = [
        state
        for state in service._ALL_STATES
        if state is not service.ServiceState.STARTING
    ]

    for state in states:
        test_service.state = state
        with pytest.raises(service.ServiceStateError):
            test_service._validate_state_for_setting(service.ServiceState.STARTED)

    try:
        test_service.state = service.ServiceState.STARTING
        test_service._validate_state_for_setting(service.ServiceState.STARTED)
    except service.ServiceStateError as e:
        pytest.fail("There should be no ServiceStateError: {!r}".format(e))


def test_validate_state_for_setting_ordering_starting(test_service):
    allowed_states = [
        service.ServiceState.UNKNOWN,
        service.ServiceState.STOPPED,
        service.ServiceState.STARTED,
    ]

    states = [
        state
        for state in service._ALL_STATES
        if state not in allowed_states
    ]

    for state in states:
        test_service.state = state
        with pytest.raises(service.ServiceStateError):
            test_service._validate_state_for_setting(service.ServiceState.STARTING)

    try:
        for state in allowed_states:
            test_service.state = state
            test_service._validate_state_for_setting(service.ServiceState.STARTING)
    except service.ServiceStateError as e:
        pytest.fail("There should be no ServiceStateError: {!r}".format(e))


def test_validated_state_for_setting_ordering_stopped(test_service):
    allowed_states = [
        service.ServiceState.STOPPING,
        service.ServiceState.FAILED,
    ]

    states = [
        state
        for state in service._ALL_STATES
        if state not in allowed_states
    ]

    for state in states:
        test_service.state = state
        with pytest.raises(service.ServiceStateError):
            test_service._validate_state_for_setting(service.ServiceState.STOPPED)

    try:
        for state in allowed_states:
            test_service.state = state
            test_service._validate_state_for_setting(service.ServiceState.STOPPED)
    except service.ServiceStateError as e:
        pytest.fail("There should be no ServiceStateError: {!r}".format(e))


def test_validated_state_for_setting_ordering_stopping(test_service):
    allowed_states = [
        service.ServiceState.STARTED,
        service.ServiceState.STARTING,
    ]

    states = [
        state
        for state in service._ALL_STATES
        if state not in allowed_states
    ]

    for state in states:
        test_service.state = state
        with pytest.raises(service.ServiceStateError):
            test_service._validate_state_for_setting(service.ServiceState.STOPPING)

    try:
        for state in allowed_states:
            test_service.state = state
            test_service._validate_state_for_setting(service.ServiceState.STOPPING)
    except service.ServiceStateError as e:
        pytest.fail("There should be no ServiceStateError: {!r}".format(e))


@pytest.mark.asyncio
async def test_start_already_started(test_service, mocker, amock):
    on_startup_mocked = mocker.patch.object(
        test_service, 'on_startup', amock())

    for ignored_state in [service.ServiceState.STARTING, service.ServiceState.STARTED]:
        test_service.state = ignored_state
        await test_service.start()

    on_startup_mocked.assert_not_called()


@pytest.mark.asyncio
async def test_start_calls_on_startup(test_service, mocker, amock):
    on_startup_mocked = mocker.patch.object(
        test_service, 'on_startup', amock())

    await test_service.start()

    on_startup_mocked.assert_called_once()
    assert test_service.state is service.ServiceState.STARTED


@pytest.mark.asyncio
async def test_stop_already_stopped(test_service, mocker, amock):
    on_stopping_mocked = mocker.patch.object(
        test_service, 'on_stopping', amock())

    for ignored_state in [service.ServiceState.STOPPING, service.ServiceState.STOPPED]:
        test_service.state = ignored_state
        await test_service.stop()

    on_stopping_mocked.assert_not_called()


@pytest.mark.asyncio
async def test_stop_calls_on_stopping(test_service, mocker, amock):
    on_stopping_mocked = mocker.patch.object(
        test_service, 'on_stopping', amock())

    test_service.state = service.ServiceState.STARTED
    await test_service.stop()

    on_stopping_mocked.assert_called_once()
    assert test_service.state is service.ServiceState.STOPPED
