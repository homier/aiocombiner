import os

import pytest

from aiocombiner.settings import SettingsSchema, fields


def test_normalize_environment_prefix_use_class_name(test_settings):
    test_settings.environment_prefix = None

    test_settings.normalize_environment_prefix()

    assert test_settings.environment_prefix == test_settings.__class__.__name__


def test_normalize_environment_prefix_invalid_type(test_settings):
    test_settings.environment_prefix = 123

    with pytest.raises(TypeError):
        test_settings.normalize_environment_prefix()


def test_normalize_environment_prefix_set_is_not_changed(test_settings):
    old = test_settings.environment_prefix

    test_settings.normalize_environment_prefix()

    assert test_settings.environment_prefix == old


def test_add_env_variables_to_schema_input_without_fields(test_settings):
    assert not test_settings.add_env_variables_to_schema_input({})


def test_add_env_variables_to_schema_input_calls_normalization(test_settings, mocker):
    normalization_mocked = mocker.patch.object(
        test_settings, 'normalize_environment_prefix')

    test_settings.add_env_variables_to_schema_input({})

    normalization_mocked.assert_called_once()

    mocker.resetall()


def test_add_env_variables_field_missed_in_env(mocker):

    class TestSchema(SettingsSchema):
        field = fields.Int()

    mocker.patch.dict(os.environ, {})

    data = TestSchema().add_env_variables_to_schema_input({'field': 1})

    assert 'field' in data
    assert data['field'] == 1

    mocker.resetall()


def test_add_env_variables_env_has_priority(mocker):

    class TestSchema(SettingsSchema):
        field = fields.Int()

    mocker.patch.dict(os.environ, {'TESTSCHEMA_FIELD': '1'})

    data = TestSchema().add_env_variables_to_schema_input({'field': 123})

    assert 'field' in data
    assert data['field'] == '1'

    mocker.resetall()
