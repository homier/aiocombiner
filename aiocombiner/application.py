import abc
import asyncio
import concurrent.futures
import contextlib
import functools
import logging
import os
import typing

import aiohttp.web
import yaml

from . import environ
from . import exception
from . import registry
from . import service
from . import util


__all__ = [
    'Application',
    'MiddlewareList',
    'RouteList',
]


DEFAULT_APPLICATION_BINDING_HOST = 'localhost'
DEFAULT_APPLICATION_BINDING_PORT = 8000


logger = logging.getLogger(__name__)


MiddlewareList = typing.List[typing.Awaitable]
RouteList = typing.List[aiohttp.web.AbstractRouteDef]


class Application(metaclass=abc.ABCMeta):
    """Base application."""

    aiohttp_app: aiohttp.web.Application = None

    name: str = None

    # A list of application's middlewares.
    middlewares: MiddlewareList = None

    # A list of application's routes.
    routes: RouteList = None

    # A list of services to be set in application.
    services: registry.ServiceList = None

    def __init__(self):
        """Overriding aiohttp's application __init__.

        :raises: TypeError, aiocombiner.ApplicationError
        """
        self._is_running = False
        self._raw_settings: typing.Dict = {}

        if self.name is None or not self.name:
            raise exception.ApplicationError("Application's name has to be provided.")

        if self.routes is None:
            self.routes = []

        if self.services is None:
            self.services = []

        util.check_type('name', self.name, str)

        self._loop: asyncio.AbstractEventLoop = None
        self._main_task: asyncio.Future = None
        self._service_registry: registry.ServiceRegistry = registry.ServiceRegistry()
        self._service_tasks = None

        self._host: str = None
        self._port: str = None

    def run(self) -> typing.NoReturn:
        """Application's running method.

        This method will perform all the initialized actions and will
        block an application.
        """
        self._is_running = True
        self._init()
        logger.info(
            "Application has been initialized with {!r} pid.".format(self.app_pid))

        self._start_services()
        self._set_main_task()

        aiohttp.web.run_app(self.aiohttp_app, host=self._host, port=self._port)

    def _init(self) -> typing.NoReturn:
        self._init_loop()
        self._init_aiohttp_application()
        self._init_routes()
        self._init_settings()
        self._init_services()

    def _init_loop(self) -> typing.NoReturn:
        """Initializes asyncio event loop.

        If you'd like to use uvloop, just set the `USE_UVLOOP` env variable.
        """
        self._set_uvloop_policy()
        self._set_loop()
        self._set_thread_pool_executor()

        logger.debug("Event loop has been initialized.")

    def _set_uvloop_policy(self) -> typing.NoReturn:
        """Sets uvloop policy to the event loop."""
        if not environ.ENV.bool('USE_UVLOOP', False):
            return

        try:
            import uvloop
            logger.debug("Setting 'uvloop' policy to the event loop...")
            asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        except ImportError:
            logger.warning("Could not import uvloop. Using default.")

    def _set_loop(self) -> typing.NoReturn:
        self._loop = asyncio.get_event_loop()

    def _set_thread_pool_executor(self) -> typing.NoReturn:
        logger.debug("Initializing 'ThreadPoolExecutor'...")
        self._loop.set_default_executor(concurrent.futures.ThreadPoolExecutor())
        logger.debug("'ThreadPoolExecutor' has been set to the event loop.")

    def _init_aiohttp_application(self) -> typing.NoReturn:
        self.aiohttp_app = aiohttp.web.Application(
            logger=None, middlewares=self.middlewares)
        self.aiohttp_app.combiner = self
        self.aiohttp_app.on_cleanup.append(self.on_app_cleanup)

    def _init_routes(self) -> typing.NoReturn:
        """Appends application's routes into the aiohttp application.

        :raises: TypeError
        """
        for route in self.routes:
            util.check_type('route', route, aiohttp.web.AbstractRouteDef)

        self.aiohttp_app.add_routes(self.routes)

    def _init_settings(self) -> typing.NoReturn:
        self._load_settings_from_file()
        self._init_base_application_settings()

    def _load_settings_from_file(self) -> typing.NoReturn:
        settings_path = environ.ENV('AIOCOMBINER_SETTINGS_PATH', None)

        if settings_path is None or not os.path.exists(settings_path):
            logger.warning(
                "Settings file was not provided. "
                "All settings will be set from environment variables. "
                "To use settings file set 'AIOCOMBINER_SETTINGS_PATH' env variable."
            )
            return

        self._load_raw_settings(settings_path)

    def _load_raw_settings(self, path: str) -> typing.NoReturn:
        with open(path) as settings_stream:
            self._raw_settings = yaml.safe_load(settings_stream)

    def _init_base_application_settings(self) -> typing.NoReturn:

        with environ.environ(self.name) as env:
            environment_host = env('HOST', None)
            environment_port = env('PORT', None)

        settings_host = self._raw_settings.get('host', DEFAULT_APPLICATION_BINDING_HOST)
        settings_port = self._raw_settings.get('port', DEFAULT_APPLICATION_BINDING_PORT)

        if environment_host is not None and environment_host:
            self._host = environment_host
        else:
            self._host = settings_host

        if environment_port is not None:
            self._port = int(environment_port)
        else:
            self._port = int(settings_port)

    def _init_services(self) -> typing.NoReturn:

        if self.services is None or not self.services:
            logger.debug("Application does not contain any service.")
            return

        for service_class in self.services:
            self._init_service(service_class)

    def _init_service(self, service_class: service.Service) -> typing.NoReturn:
        service_instance: service.Service = service_class()

        service_instance.set_application(self)
        if service_instance.is_configurable:
            self._configure_service(service_instance)

        self._service_registry[service_instance.name] = service_instance

    def _configure_service(self, service_instance) -> typing.NoReturn:
        service_settings = self._raw_settings.get(service_instance.name.lower(), None)
        if service_settings is None:
            logger.debug(
                "Settings for {!r} was not provided in file.".format(
                    service_instance.name)
            )

        service_instance.set_settings(initial_settings=service_settings)

    def _start_services(self) -> typing.NoReturn:
        logger.debug("Starting services...")
        futures = [
            service_instance.start()
            for service_instance in self._service_registry.services()
        ]
        self._service_tasks = asyncio.gather(*futures)
        self._loop.run_until_complete(self._service_tasks)
        logger.debug("Services have been started.")

    def _set_main_task(self) -> typing.NoReturn:
        logger.debug("Getting 'main' task...")
        self._main_task = asyncio.ensure_future(self.main())
        self._main_task.add_done_callback(self._handle_main_error)

    def _handle_main_error(self, fut: asyncio.Future) -> typing.NoReturn:
        try:
            fut.result()
        except Exception as e:
            logger.exception("Error in 'main': {!r}.".format(e))

    async def run_in_thread(self, func: typing.Callable, *args, **kwargs):
        """Runs provided function in a thread pool and returns result."""
        function_to_be_called = functools.partial(func, *args, **kwargs)
        return await self._loop.run_in_executor(None, function_to_be_called)

    @property
    def app_pid(self) -> int:
        return os.getpid()

    def __repr__(self) -> str:
        return '<Application name={!r}; is_running={!r}; pid={!r}>'.format(
            self.name, self._is_running, self.app_pid)

    async def on_app_cleanup(self, application: 'Application') -> typing.NoReturn:
        """On app cleanup handler"""
        if not self._is_running:
            logger.warning("The application is not running.")
            return

        self._is_running = False

        await self._shutdown_services()
        await self._shutdown_pending_tasks()

    async def _shutdown_services(self) -> typing.NoReturn:
        logger.info("Shutting down services...")

        try:
            futures = [
                service_instance.stop()
                for service_instance in self._service_registry.services()
            ]
            await asyncio.gather(*futures)
            logger.info("Services have been stopped.")
        except asyncio.CancelledError:
            pass
        except Exception:
            logger.exception("Failed to stop service")

    async def _shutdown_pending_tasks(self) -> typing.NoReturn:
        # Cancel and wait for pending tasks to complete
        pending_tasks = asyncio.Task.all_tasks()
        try:
            logger.debug("Pending tasks: {}".format(pending_tasks))
            logger.info("Waiting for pending tasks to complete...")
            for task in pending_tasks:
                task.cancel()
                with contextlib.suppress(asyncio.CancelledError):
                    await task
        except Exception as e:
            logger.exception(e)

    async def main(self) -> typing.NoReturn:
        """Override this method to put your post-startup application code here."""
        pass
