import contextlib
import typing

import environs


__all__ = [
    'ENV',
    'EnvError',
    'environ',
    'transform_prefix',
]


ENV = environs.Env()
EnvError = environs.EnvError


def transform_prefix(prefix: str) -> str:
    if not prefix.endswith('_'):
        prefix += '_'

    return prefix.upper()


@contextlib.contextmanager
def environ(prefix: str) -> typing.Generator[ENV, None, None]:
    prefix = transform_prefix(prefix)

    with ENV.prefixed(prefix):
        yield ENV
