import logging
import typing

import marshmallow

from . import environ
from . import util


__all__ = [
    'SettingsSchema',
    'UnmarshalResult',
    'fields',
]


logger = logging.getLogger(__name__)

UnmarshalResult = marshmallow.UnmarshalResult
fields = marshmallow.fields


class SettingsSchema(marshmallow.Schema):

    environment_prefix: str = None

    @marshmallow.pre_load
    def add_env_variables_to_schema_input(self, in_data: typing.Dict) -> typing.Dict:
        """Overrides initial values with values from environment.

        :raises: TypeError
        """
        self.normalize_environment_prefix()

        for field in self.fields:
            with environ.environ(self.environment_prefix) as env:
                try:
                    env_value = env(field.upper())
                except environ.EnvError:
                    continue

            in_data[field] = env_value

        return in_data

    def normalize_environment_prefix(self) -> typing.NoReturn:
        """Normalizes environment prefix.

        If environment prefix is not set for schema, this method
        will set class's name as environment prefix.
        :raises: TypeError
        """
        if self.environment_prefix is None or not self.environment_prefix:
            logger.warning(
                "Environment prefix was not set to the {name!r} class. "
                "Class name {name!r} will be used instead.".format(
                    name=self.__class__.__name__),
            )
            self.environment_prefix = self.__class__.__name__

        util.check_type("environment_prefix", self.environment_prefix, str)

    class Meta:
        # We don't want to handle errors manually.
        # So if any error occurs on schema's loading,
        # the load method raises 'ValidationError' immediatly.
        strict = True
