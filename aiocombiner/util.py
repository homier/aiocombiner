import typing


__all__ = [
    'check_type',
]


def check_type(name: str, item: typing.Any, *types) -> typing.NoReturn:
    """Checks item's type.

    :raises: TypeError
    """
    if isinstance(item, types):
        return

    raise TypeError(
        "'{}' type should be one of {}, not '{}'.".format(
            name, ', '.join(str(type_) for type_ in types), type(item))
    )
