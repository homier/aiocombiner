import abc
import collections
import enum
import logging
import typing

from .. import settings

from ..util import check_type


__all__ = [
    'Service',
    'ServiceError',
    'ServiceState',
    'ServiceStateError',
]


logger = logging.getLogger(__name__)


class ServiceError(Exception):
    pass


class ServiceStateError(ServiceError):
    """Raises on incorrect service state."""


class ServiceState(enum.Enum):
    """Designates a service's current state."""

    FAILED: str = 'FAILED'
    STARTED: str = 'STARTED'
    STARTING: str = 'STARTING'
    STOPPED: str = 'STOPPED'
    STOPPING: str = 'STOPPING'
    UNKNOWN: str = 'UNKNOWN'


_ALL_STATES = [
    ServiceState.FAILED,
    ServiceState.STARTED,
    ServiceState.STARTING,
    ServiceState.STOPPED,
    ServiceState.STOPPING,
    ServiceState.UNKNOWN,
]


# Defines the correct state changing order.
_CORRECT_STATE_CHANGING_ORDER = collections.defaultdict(
    list, {
        ServiceState.UNKNOWN: _ALL_STATES,
        ServiceState.STARTING: [
            ServiceState.UNKNOWN,
            ServiceState.STOPPED,
            ServiceState.STARTED,
        ],
        ServiceState.STARTED: [
            ServiceState.STARTING,
        ],
        ServiceState.STOPPING: [
            ServiceState.STARTED,
            ServiceState.STARTING,
        ],
        ServiceState.STOPPED: [
            ServiceState.STOPPING,
            ServiceState.FAILED,
        ],
        ServiceState.FAILED: [
            ServiceState.STARTED,
        ],
    },
)


class Service(metaclass=abc.ABCMeta):
    """Base service class.

    'on_startup' and 'on_stopping' methods have to be implemented.
    """

    name: str = None

    # Set it to True if you want to make cofigurable service.
    is_configurable: bool = False
    settings_schema: settings.SettingsSchema = None
    settings_data: settings.UnmarshalResult = None

    def __init__(self):
        """Implementing __init__ method.

        :raises: AssertionError, TypeError
        """
        assert self.name is not None and self.name

        if self.is_configurable:
            assert self.settings_schema is not None
            check_type('settings_schema', self.settings_schema, settings.SettingsSchema)

        check_type('name', self.name, str)

        self.application = None
        self.state = ServiceState.UNKNOWN

    def set_application(self, application) -> typing.NoReturn:
        """Sets parent application to the service.

        :type application: aiocombiner.Application
        """
        self.application = application

    def set_state(self, state: str) -> typing.NoReturn:
        """Sets a new state for the service."""
        self._validate_state_for_setting(state)

        logger.info(
            "Service {!r} has changed its state from {!r} to {!r}".format(
                self.name, self.state, state),
        )
        self.state = state

    def set_settings(self, initial_settings: typing.Dict = None) -> typing.NoReturn:
        if not self.is_configurable:
            logger.warning("Service {!r} is not configurable.".format(self.name))
            return

        self._set_settings(initial_settings=initial_settings)

    def _set_settings(self, initial_settings: typing.Dict = None) -> typing.NoReturn:
        if initial_settings is None:
            initial_settings = dict()

        check_type('initial_settings', initial_settings, typing.Dict)

        self.settings_data, _ = self.settings_schema.load(initial_settings)

    def _validate_state_for_setting(self, state: str) -> typing.NoReturn:
        if self.state not in _CORRECT_STATE_CHANGING_ORDER[state]:
            raise ServiceStateError(
                "Incorrect changing state for {!r}: {!r} -> {!r}".format(
                    self.name, self.state, state),
            )

    async def start(self) -> typing.NoReturn:
        """Service's start method.

        If you'd like to extend startup actions, override or implement
        `on_startup` method.
        """
        if self.state in [ServiceState.STARTING, ServiceState.STARTED]:
            logger.warning(
                "Service {!r} is already run or running.".format(self.name))
            return

        self.set_state(ServiceState.STARTING)
        await self.on_startup()
        self.set_state(ServiceState.STARTED)

    async def stop(self) -> typing.NoReturn:
        """Service's stop method.

        If you'd like to extend stopping actions, override or implement
        `on_stopping` method.
        """
        if self.state in [ServiceState.STOPPING, ServiceState.STOPPED]:
            logger.warning(
                "Service {!r} is already stopped or stopping.".format(self.name))
            return

        self.set_state(ServiceState.STOPPING)
        await self.on_stopping()
        self.set_state(ServiceState.STOPPED)

    @abc.abstractmethod
    async def on_startup(self) -> typing.NoReturn:
        """Service's on startup callback."""
        raise NotImplementedError

    @abc.abstractmethod
    async def on_stopping(self) -> typing.NoReturn:
        """Service's on stopping callback."""
        raise NotImplementedError

    def __repr__(self) -> str:
        return '<Service name={!r}; state={!r}>'.format(self.name, self.state)
