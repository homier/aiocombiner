import typing

from . import service
from . import util

from .exception import ApplicationError


__all__ = [
    'ServiceList',
    'ServiceRegistry',
]


ServiceList = typing.List[service.Service]


class ServiceRegistry(dict):
    """Application's service registry."""

    def __getitem__(self, key) -> service.Service:
        """Overriding getitem.

        :raises: aiocombiner.ApplicationError
        """
        try:
            return super().__getitem__(key)
        except KeyError as e:
            raise ApplicationError("{!r} is not in service registry.".format(key)) from e

    def __setitem__(self, key, value: service.Service) -> typing.NoReturn:
        """Overriding setitem.

        :raises: aiocombiner.ApplicationError, TypeError
        """
        if key in self:
            raise ApplicationError(
                "{!r} service is already registered.".format(key))

        util.check_type('service', value, service.Service)

        super().__setitem__(key, value)

    def services(self) -> ServiceList:
        return list(self.values())
