
__all__ = [
    'ApplicationError',
]


class ApplicationError(Exception):
    pass
