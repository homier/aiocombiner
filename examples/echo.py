#!/usr/bin/env python3
import asyncio
import logging
import os
import typing

import aiohttp.web

from aiocombiner import application
from aiocombiner import registry
from aiocombiner import service
from aiocombiner import settings


__all__ = [
    'EchoApplication',
]


logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] -- %(message)s')
logger = logging.getLogger()


async def _echo_route(request: aiohttp.web.Request) -> aiohttp.web.Response:
    return aiohttp.web.json_response(
        {'echo': 'It works for the {!r}!'.format(request.app.combiner)})


async def _echo_ws_route(request: aiohttp.web.Request) -> aiohttp.web.WebSocketResponse:
    logger.debug("Accepting web socket connection...")
    ws_response = aiohttp.web.WebSocketResponse()

    await ws_response.prepare(request)

    request.app.combiner.echo.connections.add(ws_response)

    async for message in ws_response:
        if message.type == aiohttp.WSMsgType.TEXT:
            if message.data == 'close':
                await ws_response.close()
        elif message.type == aiohttp.WSMsgType.ERROR:
            logger.error(
                "WS connection closed with exception {!r}".format(
                    ws_response.exception()),
            )

    request.app.combiner.echo.connections.remove(ws_response)
    logger.debug("Web socket connection closed.")

    return ws_response


class EchoSettingsSchema(settings.SettingsSchema):

    environment_prefix = 'echo'
    sleep_delay = settings.fields.Int(required=True)


class EchoService(service.Service):

    name = 'echo'
    is_configurable = True
    settings_schema = EchoSettingsSchema()

    connections: set = None

    async def on_startup(self) -> typing.NoReturn:
        self.application.echo = self
        self.connections = set()
        logger.info("{!r} is started for {!r}".format(self, self.application))

        self.notify_ws_task = asyncio.ensure_future(self.notify_ws())

    async def on_stopping(self) -> typing.NoReturn:
        logger.info("{!r} is stopping".format(self))

        self.notify_ws_task.cancel()
        try:
            await self.notify_ws_task
        except asyncio.CancelledError:
            pass

    async def notify_ws(self) -> typing.NoReturn:
        # NOTE: This is just an example
        while True:
            for ws_connection in self.connections:
                await ws_connection.send_str(
                    'It works for the {!r}!'.format(self))
            await asyncio.sleep(self.settings_data['sleep_delay'])


class EchoApplication(application.Application):

    name: str = 'echo'

    routes: application.RouteList = [
        aiohttp.web.get('/echo', _echo_route, name='echo-route'),
        aiohttp.web.get('/echo-ws', _echo_ws_route, name='echo-ws-route'),
    ]

    services: registry.ServiceList = [
        EchoService,
    ]

    echo: EchoService = None

    async def main(self) -> typing.NoReturn:
        logger.info('{!r} is working with {!r} echo service!'.format(self, self.echo))


def main():
    os.environ['AIOCOMBINER_SETTINGS_PATH'] = os.path.join(
        os.path.dirname(__file__), 'echo_settings.yaml')
    os.environ.setdefault('ECHO_HOST', '0.0.0.0')
    app = EchoApplication()
    app.run()


if __name__ == '__main__':
    main()
